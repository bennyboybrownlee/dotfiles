
local utils = require 'mp.utils'
local msg = require 'mp.msg'

function append(primary)
    local args = { 'xclip', '-selection', primary and 'primary' or 'clipboard', '-out' }
    local clipboard = handleres(utils.subprocess({ args = args, cancellable = false }), args, primary)
    if clipboard then
        mp.commandv("loadfile", clipboard, "append-play")
        msg.info("queued video: "..clipboard)
    end
end

function handleres(res, args, primary)
  if not res.error and res.status == 0 then
      return res.stdout
  else
    if not primary then
      append(true)
      return nil
    end
    msg.error("There was an error getting clipboard: ")
    msg.error("  Status: "..(res.status or ""))
    msg.error("  Error: "..(res.error or ""))
    msg.error("  stdout: "..(res.stdout or ""))
    msg.error("args: "..utils.to_string(args))
    return nil
  end
end

mp.add_key_binding("a", "append", append)
