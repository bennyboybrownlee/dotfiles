
-- enable 24-bit colors
vim.o.termguicolors=true

require('plugins')
require('config')
require('keybinds')

-- enable mouse support
vim.o.mouse='a'

-- keep cursor centered
vim.o.scrolloff=999

-- proper character encoding
vim.o.encoding='UTF-8'

-- update time of tagbar (ms)
vim.o.updatetime=250

-- line wrapping
vim.o.wrap=false

-- enable sign column
vim.o.signcolumn='yes'

--  show substitution mid command
vim.o.inccommand='split'

-- syntax highlighting
vim.o.syntax='on'

-- underline under current line number
vim.o.cursorline=true

-- show the current mode (ie insert) in bottom left
vim.o.showmode=true

-- line numbers
vim.o.number=true
vim.o.relativenumber=true

vim.o.exrc=true

vim.o.textwidth=90

-- indentation
vim.o.autoindent=true
vim.o.expandtab=true
vim.o.shiftwidth=4
vim.o.softtabstop=4
vim.o.tabstop=4

vim.o.incsearch=true

vim.o.wildmenu=true
vim.o.ruler=true
vim.o.showcmd=true
vim.o.ignorecase=true
vim.o.smartcase=true
vim.o.laststatus=2
vim.o.showmatch=true

-- backup files
vim.o.backup=false
vim.o.swapfile=false
vim.o.writebackup=false

-- source legacy configuration settings
vim.cmd 'source ~/.config/nvim/vim/init.vim'
