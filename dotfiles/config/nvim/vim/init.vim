
function! ApplyHighlights()
    " remove regular todo highlights
    highlight clear todo
    highlight link todo comment
    highlight Normal ctermbg=none guibg=none
    highlight NonText ctermbg=none guibg=none

    " configure LSP warning colors
    "highlight LspDiagnosticsSignError guifg=red
    "highlight LspDiagnosticsDefaultError guifg=red
    "highlight LspDiagnosticsVirtualTextError guifg=red
    "highlight LspDiagnosticsFloatingError guifg=red

    "highlight LspDiagnosticsSignInformation guifg=yellow
    "highlight LspDiagnosticsDefaultInformation guifg=yellow
    "highlight LspDiagnosticsVirtualTextInformation guifg=yellow
    "highlight LspDiagnosticsFloatingInformation guifg=yellow

    "highlight LspDiagnosticsSignHint guifg=grey
    "highlight LspDiagnosticsDefaultHint guifg=grey
    "highlight LspDiagnosticsVirtualTextHint guifg=grey
    "highlight LspDiagnosticsFloatingHint guifg=grey

endfunction

function! GitBranch()
  return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endfunction

function! StatuslineGit()
  let l:branchname = GitBranch()
  return strlen(l:branchname) > 0?'   '.l:branchname:''
endfunction

augroup on_colorscheme_load
    autocmd!
    autocmd ColorScheme * call ApplyHighlights()
augroup end

augroup vimrc_help
  autocmd!
  autocmd BufEnter *.txt if &buftype == 'help' | wincmd L | endif
augroup END

set laststatus=2

set statusline=
set statusline+=%{StatuslineGit()}
set statusline+=\ %f
set statusline+=%m
set statusline+=%=
set statusline+=\ %y
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=\ \[%{&fileformat}\]
set statusline+=\ %p%%
set statusline+=\ %l:%c
set statusline+=\ 

au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

