" more down pane
noremap <C-J> <C-W><C-J>
" more up pane
noremap <C-K> <C-W><C-K>
" move right pane
noremap <C-L> <C-W><C-L>
" move left pane
noremap <C-H> <C-W><C-H>
" close buffer
noremap <C-Q> <C-W><C-Q>
" open new tab
nnoremap <C-W>t :tabnew<CR>
nnoremap <C-W><C-t> :tabnew<CR>
" next tab
nnoremap <Tab> gt
" prev tab
nnoremap <S-Tab> gT

" comment toggle
map <C-_> <plug>NERDCommenterToggle
" open ranger
noremap <Leader>r :FloatermNew ranger<CR>
" open repgrep
noremap <Leader>g :Rg<CR>
noremap <Leader>wg :Rg <C-R><C-W><CR>
noremap <Leader>vg y:Rg <C-R>"<CR>
" open fzf
noremap <Leader>f :FZF<CR>
"clear highlight
"noremap <Leader>h :noh<CR>

"noremap j gj
"noremap k gk
nmap gx :silent execute "!xdg-open " . shellescape("<cWORD>")<CR>

