let g:gruvbox_transparent_bg = 1

let g:NERDCreateDefaultMappings = 0
let g:NERDRemoveExtraSpaces = 0

let g:tex_flavor = 'latex'
let g:vimtex_quickfix_mode = 1
let g:vimtex_view_general_viewer = 'zathura'
let g:vimtex_quickfix_ignore_filters = [
    \ 'Package Fancyhdr Warning',
    \ 'LaTeX Warning: Command \O invalid in math mode',
    \ 'Command \O invalid in math mode',
    \ 'Package amsmath Warning',
    \]

let g:floaterm_position = 'center'
let g:floaterm_winblend = 30
let g:floaterm_height   = 0.7
let g:floaterm_width    = 0.7
let g:floaterm_opener   = 'edit'
let g:floaterm_keymap_toggle = '<F12>'

