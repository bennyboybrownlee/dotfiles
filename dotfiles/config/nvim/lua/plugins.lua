
return require('packer').startup(function()

    -- plugin manager
    use ('wbthomason/packer.nvim')

    -- lsp server
    use ('neovim/nvim-lspconfig')

    -- completion engine
    use ('hrsh7th/nvim-compe')

    -- telescope finder
    use ('nvim-telescope/telescope.nvim')

    use ('nvim-lua/plenary.nvim')

    -- git signs
    use {
        'lewis6991/gitsigns.nvim',
        requires = {
            'nvim-lua/plenary.nvim'
        }
    }

    -- git integration
    use ('tpope/vim-fugitive')
    use ('tpope/vim-rhubarb')
    use ('shumphrey/fugitive-gitlab.vim')


    use {
        'folke/todo-comments.nvim',
        requires = "nvim-lua/plenary.nvim",
    }

    use ('folke/trouble.nvim')

    -- latex tools
    use {
        'lervag/vimtex',
        ft = {'tex','latex'}
    }

    -- auto brace pairing
    use ('jiangmiao/auto-pairs')

    -- nord theme
    use ('arcticicestudio/nord-vim')

    -- iceberg theme
    use ('cocopon/iceberg.vim')

    use ('morhetz/gruvbox')

    -- gdb debugging integration
    use {
        'sakhnik/nvim-gdb',
        ft =  {'c','cpp','python'},
        run = "./install.sh"
    }

    -- fzf integration
    use ('junegunn/fzf.vim')

    -- floating terminal
    use ('voldikss/vim-floaterm')

    -- universal comment control
    use ('scrooloose/nerdcommenter')

    -- 
    --use ('psf/black')

    use ('luochen1990/rainbow')

    -- lint engine
    --use ('dense-analysis/ale')

end)
