# z shell init file
# =================
# see docs: https://zsh.sourceforge.io/Doc/Release/index.html#Top

# p10k prompt
# see docs: https://github.com/romkatv/powerlevel10k

if [[ "$TERM" != "linux" ]]; then 
    if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
      source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
    fi
fi


# word delimination

export WORDCHARS='*?_-.[]~=/&;!#$%^(){}<>:,"'"'"
autoload -U select-word-style
select-word-style bash


# history
# see docs: https://zsh.sourceforge.io/Doc/Release/Options.html#History

HISTFILE=~/.histfile
HISTSIZE=5000
SAVEHIST=5000
setopt INC_APPEND_HISTORY        # write history immeadiately
setopt SHARE_HISTORY             # share history in all sessions
setopt HIST_IGNORE_DUPS          # ignore duplicates
setopt HIST_IGNORE_ALL_DUPS      # ignore duplicates
setopt HIST_FIND_NO_DUPS         # ignore duplicates
setopt HIST_SAVE_NO_DUPS         # ignore duplicates
setopt HIST_IGNORE_SPACE         # trim extra whitespace
setopt HIST_REDUCE_BLANKS        # trim extra whitespace
setopt interactivecomments       # allow typing comments at an interactive prompt
setopt correct                   # suggest spelling suggestions for mistyped commands


# firefox send
# see docs: https://github.com/timvisee/ffsend

export FFSEND_HOST="https://send.brownlee.dev"

# completions

zstyle :compinstall filename '/home/benjamin/.zshrc'
autoload -Uz compinit
compinit

# keybinds
# see docs: https://zsh.sourceforge.io/Doc/Release/Zsh-Line-Editor.html
# use `showkey -a` to find character codes

bindkey '^[[H'      beginning-of-line       # home
bindkey '^[[F'      end-of-line             # end
bindkey '^[[3~'     delete-char             # del
bindkey '^U'        backward-kill-line      # ctrl+u
bindkey '^H'        backward-delete-word    # ctrl+backspace
bindkey '^[[1;5C'   forward-word            # ctrl+right
bindkey '^[[1;5D'   backward-word           # ctrl+left
bindkey '^[[Z'      autosuggest-accept      # ?
bindkey '^ '        autosuggest-execute     # ctrl+space

# theme

if [[ "$TERM" != "linux" ]]; then 
    source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme
    source /etc/profile.d/vte.sh
fi

# fzf
# see docs: https://github.com/junegunn/fzf/wiki/Configuring-shell-key-bindings

export FZF_ALT_C_COMMAND="command find -L . -mindepth 1 \\( -name node_modules -o -name venv -o -path '*/\\.*' -o -fstype 'sysfs' -o -fstype 'devfs' -o -fstype 'devtmpfs' -o -fstype 'proc' \\) -prune -o -type d -print 2> /dev/null | cut -b3-"
export FZF_CTRL_T_COMMAND="command find -L . -mindepth 1 \\( -name node_modules -o -name venv -o -path '*/\\.*' -o -fstype 'sysfs' -o -fstype 'devfs' -o -fstype 'devtmpfs' -o -fstype 'proc' \\) -prune -o -type f -print -o -type d -print -o -type l -print 2> /dev/null | cut -b3-"

source /usr/share/fzf/completion.zsh
source /usr/share/fzf/key-bindings.zsh
source ~/.config/fzf/functions.sh

rga-fzf() {
    RG_PREFIX="rga --files-with-matches"
    local file
    file="$(
        FZF_DEFAULT_COMMAND="$RG_PREFIX $1" \
        fzf --sort --preview="[[ ! -z {} ]] && rga --pretty --context 5 {q} {}" \
        --phony -q "$1" \
        --bind "change:reload:$RG_PREFIX {q}" \
        --preview-window="70%:wrap"
    )" &&
    echo "opening $file" &&
    xdg-open "$file"
}


# start ssh agent

if [ -z "$SSH_AUTH_SOCK" ] ; then
    eval `ssh-agent -s ` &> /dev/null
fi

# miscellaneous options

LANG=en_US.UTF8

# load aliases
[ -f "$HOME/.aliases" ] && source "$HOME/.aliases"    

# intialize or load p10k
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# zinit
# see docs: 

if [[ ! -f $HOME/.zinit/bin/zinit.zsh ]]; then
    print -P "%F{33}▓▒░ %F{220}Installing %F{33}DHARMA%F{220} Initiative Plugin Manager (%F{33}zdharma/zinit%F{220})…%f"
    command mkdir -p "$HOME/.zinit" && command chmod g-rwX "$HOME/.zinit"
    command git clone https://github.com/zdharma/zinit "$HOME/.zinit/bin" && \
        print -P "%F{33}▓▒░ %F{34}Installation successful.%f%b" || \
        print -P "%F{160}▓▒░ The clone has failed.%f%b"
fi
source "$HOME/.zinit/bin/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

# zinit plugins

#zinit light "MichaelAquilina/zsh-autoswitch-virtualenv"
zinit light "zsh-users/zsh-autosuggestions"
zinit light "zdharma/fast-syntax-highlighting"


